package grails_project

class TwitterUser {
    TwitterUser(String login, Role role) {
        this.login = login
        this.role = role
    }

    static constraints = {
    }

    String login
    Role role

    static hasMany = [tweets: Tweet]
}
