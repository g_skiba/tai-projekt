package grails_project

class Tweet {

    static constraints = {
    }

    String text
    String tweetId
    static belongsTo = [user: TwitterUser]

    static mapping = {
        id generator: "assigned", name: "tweetId", type: "string"
    }
}
