<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
  		<asset:stylesheet src="application.css"/>
		<g:layoutHead/>
	<style>
		body {
			padding-top: 50px;
		}
	</style>
	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">TAI Project</a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="${createLink(controller: "tweet", action: "index")}">Tweets</a></li>
						<li><a href="${createLink(controller: "user", action: "index")}">Users</a></li>
					</ul>
					<g:if test="${session["twitter:oasAccessToken"]}">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="${createLink(controller: "user", action: "logout")}">Logout</a></li>
						</ul>
					</g:if>
				</div><!--/.nav-collapse -->
			</div>
		</nav>
		<div class="container">
			<g:if test="${flash.message}">
				<div class="message alert alert-info" style="margin-top: 5px" role="status">${flash.message}</div>
			</g:if>
			<g:layoutBody/>
		</div>
		<asset:javascript src="application.js"/>
	</body>
</html>
