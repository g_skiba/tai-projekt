<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Your tweets</title>
</head>

<body>
<g:each in="${tweets}" var="tweet">
    <p style="border: 1px dotted darkgray">
        <span>"${tweet.tweetId}"</span>
        <span>"${tweet.text}"</span>
    </p>
</g:each>

<p><g:if test="${admin}">
    <g:form controller="tweet" action="save">
        <label>Tweet something: </label>
        <g:textField name="text"/><br/>
        <g:actionSubmit class="btn btn-lg btn-primary" value="Save"/>
    </g:form>
</g:if></p>


</body>
</html>