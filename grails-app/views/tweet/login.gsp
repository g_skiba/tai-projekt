<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Login page</title>
    <meta name="layout" content="main">
    <style type="text/css" media="screen">
        .centered {
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="centered">
        <oauth:connect class="btn btn-lg btn-primary" provider="twitter">Log in with Twitter</oauth:connect>
    </div>
</body>
</html>