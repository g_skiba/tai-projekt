<%@ page contentType="text/html;charset=UTF-8" %>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>

<body>

<div class="col-lg-12">
    <table class="table table-striped text-center">
        <thead>
        <tr>
            <td>Login</td>
            <td>Role</td>
            <g:if test="${admin}">
                <td>Delete?</td>
            </g:if>
        </tr>
        </thead>
        <tbody>
            <g:each in="${users}" var="user">
                <tr>
                    <td>${user.login}</td>
                    <td>${user.role}</td>
                    <g:if test="${admin}">
                        <td>
                            <g:form controller="user" action="delete">
                                <g:hiddenField name="user" value="${user.login}" />
                                <button type="submit" class="btn btn-default btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </g:form>
                        </td>
                    </g:if>
                </tr>
            </g:each>
        </tbody>
    </table>
</div>

<p><g:if test="${admin}">
    <g:form controller="user" action="add">
        <label>Add admin: </label>
        <g:textField name="login"/><br/>
        <g:actionSubmit class="btn btn-lg btn-primary" value="Add"/>
    </g:form>
</g:if></p>

</body>
</html>