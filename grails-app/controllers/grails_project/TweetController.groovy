package grails_project

import grails.converters.JSON
import uk.co.desirableobjects.oauth.scribe.OauthService

import static grails_project.Helpers.getCurrentUsername
import static grails_project.Helpers.isAdmin

class TweetController {
    OauthService oauthService

    def index() {
        def username = getCurrentUsername(oauthService, session)
        [tweets: Tweet.list(), admin: isAdmin(username)]
    }

    def save() {
        def tweet = new Tweet(params)
        def username = getCurrentUsername(oauthService, session)
        if(!isAdmin(username)) {
            flash.message = "You're not an admin!"
            redirect(controller: "user", action: "index")
            return false
        }
        def twitterResource = oauthService.postTwitterResource(twitterAccessToken,
                "https://api.twitter.com/1.1/statuses/update.json?status=${tweet.text}")

        def twitterResponse = JSON.parse(twitterResource?.getBody())

        tweet.setTweetId(twitterResponse.id_str)
        tweet.setUser(TwitterUser.findByLogin(username))
        tweet.save(flush: true)

        redirect(action: "index")
    }

    def login() {
    }

    def show() {
        def tweets = Tweet.list()
        [tweets: tweets]
    }
}
