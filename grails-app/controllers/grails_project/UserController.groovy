package grails_project

import uk.co.desirableobjects.oauth.scribe.OauthService

import static grails_project.Helpers.getCurrentUsername
import static grails_project.Helpers.isAdmin

class UserController {
    OauthService oauthService

    def index() {
        def users = TwitterUser.list()
        def username = getCurrentUsername(oauthService, session)

        [users: users, admin: isAdmin(username)]
    }

    def add() {
        String login = params.login
        def username = getCurrentUsername(oauthService, session)

        if(!isAdmin(username)) {
            flash.message = "You're not an admin!"
            redirect(controller: "user", action: "index")
            return false
        }
        def user = TwitterUser.findByLogin(login)
        if(user!=null) {
            user.setRole(Role.ADMIN)
            user.save(flush: true)
        }
        else {
            new TwitterUser(login, Role.ADMIN).save(flush: true)
        }

        flash.message = "Ok, everything's good!"
        redirect(controller: "user", action: "index")
    }
    def delete() {
        def username = getCurrentUsername(oauthService, session)
        if(!isAdmin(username)) {
            flash.message = "You're not an admin!"
            redirect(controller: "user", action: "index")
            return false
        }
        def user = TwitterUser.findByLogin(params.user)
        if(user!=null) {
            user.delete(flush: true)
        }
        if(user.role==Role.ADMIN) {
            flash.message = "${params.user} - admin permission revoked!"
        }
        else {
            flash.message = "${params.user} - user deleted!"
        }
        redirect(action: "index")
    }

    def logout() {
        session[oauthService.findSessionKeyForAccessToken('twitter')] = null
        redirect(controller: "tweet", action: "index")
    }
}
