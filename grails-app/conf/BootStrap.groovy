import grails_project.Role
import grails_project.TwitterUser

class BootStrap {

    def init = { servletContext ->
        if(TwitterUser.count() == 0 ) {
            new TwitterUser("wojakows", Role.ADMIN).save()
//            new User("twitJohnSmith1", Role.ADMIN).save()
        }
    }
    def destroy = {
    }
}
