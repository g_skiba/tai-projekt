import uk.co.desirableobjects.oauth.scribe.OauthService

class LoginFilters {
    OauthService oauthService


    def filters = {
        all(controller: "*", action: "*") {
            before = {
                if(!((controllerName=="tweet" && actionName=="login") || controllerName=="oauth")) {
                    if(!session[oauthService.findSessionKeyForAccessToken('twitter')]) {
                        redirect(controller: "tweet", action: "login")
                        return false
                    }
                }
            }
            after = { Map model ->

            }
            afterView = { Exception e ->

            }
        }
    }
}
