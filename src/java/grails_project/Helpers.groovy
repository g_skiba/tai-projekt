package grails_project

import grails.converters.JSON
import org.scribe.model.Token
import uk.co.desirableobjects.oauth.scribe.OauthService

class Helpers {
    static def isAdmin(String username) {
        def user = TwitterUser.findByLogin(username)
        boolean isAdmin = false
        if(user!=null) {
            isAdmin = user.role == Role.ADMIN
        }
        else {
            user = new TwitterUser(username, Role.DEFAULT)
            user.save(flush: true)
        }

        return isAdmin
    }

    static def getCurrentUsername(OauthService oauthService, session) {
        Token twitterAccessToken = session[oauthService.findSessionKeyForAccessToken('twitter')]
        def userResource = oauthService.getTwitterResource(twitterAccessToken,
                "https://api.twitter.com/1.1/account/verify_credentials.json")
        def username = JSON.parse(userResource?.getBody()).screen_name

        return username
    }
}
